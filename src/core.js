
_ = new Object();


var gets = (function() {
    var a = window.location.search;
    var b = new Object();
    a = a.substring(1).split("&");
    for (var i = 0; i < a.length; i++) {
  	c = a[i].split("=");
        b[c[0]] = c[1];
    }
    return b;
})();

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var tmpReseller = '';
function GetResellerse(){
  $.ajax({
    type: 'POST',
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    url: 'http://office.donotwait.ru/api/v1/GetResellers',
    headers: { "Authorization": $.cookie('my_token') },
    success: function ( data ) {
      console.log(data);
      if ( data.errorCode == 7 ) {
        location.href = 'index.html?err=' + data.message;
      }
      tmpReseller = data.result;
      reseller.dataReseller = data.result;
      reseller.modalBackFirstStart = false;
    }, 
    error: function (xhr, str) {
      alert('Ошибка!');
    }
  });
  return true;
}

var _city = [{}];
function GetCities(){
  _.userLocation = '';
  $.ajax({
    type: 'POST',
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    url: 'http://office.donotwait.ru/api/v1/GetCities',
    headers: { "Authorization": $.cookie('my_token') },
    data: JSON.stringify( _ ),
    success: function (data) {
      _city = data.data;
    }, 
    error: function (xhr, str) {
      alert('Ошибка!');
    }
  });
  return false;
}


function sortPropertyName ( a, b ) {
  if ( a.eMail < b.eMail ) return 1;
  if ( a.eMail > b.eMail ) return -1;
}

function whoIsMyCity ( codeCity ) {
  var request = '';
  for( key in _city ){ 
    if ( _city[key].id == codeCity ) {
      request = _city[key].name;
    }
  }
  return request;
}

function testToken (){
  console.log( $.cookie('my_token') );
  if ( $.cookie('my_token') == '' ) {
    location.href = 'index.html';
  }
}

/*
 * $length int, length key
 * return string key
 */
function getPass ( $length ) {
  var array_rand = function (array) {
    var array_length = array.length;
    var result = Math.random() * array_length;
    return Math.floor(result);
  };
  
  var array_for_rand_pass = ["a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T", "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  var $key = "";
  for(var i = 1; i <= $length; i++) {
    $key += array_for_rand_pass[ array_rand( array_for_rand_pass ) ];
  }
  return $key;
}
/*
 * return string (GUID / uuid code)
 */
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function searchRepeatObject(a, b) {
  var result = [];
  for(var akey in a) {
    var found = false;
    for(var bkey in b) {
      if (a[akey] == b[bkey]) {
        found = true;
        continue;
      }
    }
    if (!found) {
      result.push(a[akey]);
    }
  }
  return result;
}